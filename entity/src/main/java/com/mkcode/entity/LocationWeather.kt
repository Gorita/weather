package com.mkcode.entity

import java.util.*

data class LocationWeather(
    val weatherStateName: String,
    val weatherStateAbbr: String,
    val theTemp: Double,
    val humidity: Int,
    val applicableDate: Date
)
