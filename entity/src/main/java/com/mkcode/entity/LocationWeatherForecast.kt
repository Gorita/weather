package com.mkcode.entity

data class LocationWeatherForecast(
    val locationName: String,
    val weathers: List<LocationWeather>
)
