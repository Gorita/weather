package com.mkcode.entity

data class Location(
    val title: String,
    val locationType: String,
    val woeid: Long,
    val lattLong: String
)
