package com.mkcode.domain

import io.reactivex.rxjava3.core.Scheduler

interface ScheduleProvider {
    fun ui(): Scheduler
    fun io(): Scheduler
}
