package com.mkcode.domain

import com.mkcode.entity.Location
import com.mkcode.entity.LocationWeather
import io.reactivex.rxjava3.core.Single

interface WeatherRepository {
    fun searchLocation(query: String): Single<List<Location>>
    fun getLocationWeather(woeid: Long): Single<List<LocationWeather>>
}
