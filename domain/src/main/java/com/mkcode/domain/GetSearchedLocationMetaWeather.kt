package com.mkcode.domain

import com.mkcode.entity.LocationWeatherForecast
import io.reactivex.rxjava3.core.Single

class GetSearchedLocationMetaWeather(
    private val scheduleProvider: ScheduleProvider,
    private val repository: WeatherRepository
) : GetSearchedLocationWeather {
    override fun search(query: String): Single<List<LocationWeatherForecast>> =
        repository.searchLocation(query)
            .flatMap { locations ->
                Single.zip(
                    locations.map { location ->
                        repository.getLocationWeather(location.woeid)
                            .map {
                                LocationWeatherForecast(
                                    locationName = location.title,
                                    weathers = it
                                )
                            }
                    }
                ) {
                    it.filterIsInstance(LocationWeatherForecast::class.java)
                }
            }
            .subscribeOn(scheduleProvider.io())
            .observeOn(scheduleProvider.ui())
}


interface GetSearchedLocationWeather {
    fun search(query: String): Single<List<LocationWeatherForecast>>
}
