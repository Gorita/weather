package com.mkcode.domain

import com.mkcode.entity.Location
import com.mkcode.entity.LocationWeather
import com.mkcode.entity.LocationWeatherForecast
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.TestObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit


class GetSearchedLocationMetaWeatherTest {
    @Mock
    private lateinit var repository: WeatherRepository
    private lateinit var getSearchedLocationWeather: GetSearchedLocationWeather
    private val testScheduler: TestScheduler = TestScheduler()
    private val testScheduleProvider = object : ScheduleProvider {
        override fun ui(): Scheduler = testScheduler
        override fun io(): Scheduler = testScheduler
    }
    private val targetQuery = "se"

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        getSearchedLocationWeather = GetSearchedLocationMetaWeather(
            testScheduleProvider,
            repository
        )
    }

    @Test
    fun search_ordering() {
        val fakeWoeidList: List<Long> = listOf(1, 2, 3, 4, 5)
        mockSearchLocation(fakeWoeidList)
        mockGetLocationWeatherWithReverseDelay(fakeWoeidList)
        val testObserver: TestObserver<List<LocationWeatherForecast>> = TestObserver()

        getSearchedLocationWeather.search(targetQuery)
            .subscribe(testObserver)


        testScheduler.advanceTimeBy(fakeWoeidList.size.toLong(), TimeUnit.SECONDS)
        testObserver
            .assertValue { it.size == fakeWoeidList.size }
            .assertValue {
                it.map(LocationWeatherForecast::locationName) ==
                        fakeWoeidList.map { "title$it" }
            }
    }

    private fun mockSearchLocation(woeidList: List<Long>) {
        Mockito.`when`(repository.searchLocation(targetQuery))
            .thenReturn(
                Single.just(
                    woeidList.map {
                        Location("title$it", "", it, "")
                    }
                )
            )
    }

    private fun mockGetLocationWeatherWithReverseDelay(fakeWoeidList: List<Long>) {
        fakeWoeidList.forEach {
            Mockito.`when`(repository.getLocationWeather(it))
                .thenReturn(
                    Single.just(listOf<LocationWeather>())
                        .delay(fakeWoeidList.size - it, TimeUnit.SECONDS, testScheduler)
                )
        }
    }
}
