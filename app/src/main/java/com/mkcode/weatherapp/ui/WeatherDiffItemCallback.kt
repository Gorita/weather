package com.mkcode.weatherapp.ui

import androidx.recyclerview.widget.DiffUtil
import com.mkcode.entity.LocationWeatherForecast

object WeatherDiffItemCallback : DiffUtil.ItemCallback<LocationWeatherForecast>() {
    override fun areItemsTheSame(
        oldItem: LocationWeatherForecast,
        newItem: LocationWeatherForecast
    ): Boolean = oldItem.locationName == newItem.locationName

    override fun areContentsTheSame(
        oldItem: LocationWeatherForecast,
        newItem: LocationWeatherForecast
    ): Boolean = oldItem == newItem
}
