package com.mkcode.weatherapp.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mkcode.weatherapp.R

class TableItemDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private val dividerWidth: Int = context.resources.getDimensionPixelSize(R.dimen.divider_width)
    private val paint: Paint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.grey_300)
    }

    override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        outRect.set(
                dividerWidth,
                if (position == 0) dividerWidth
                else 0,
                dividerWidth,
                dividerWidth
        )
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        c.save()
        for (i in 0 until parent.childCount) {
            val view = parent.getChildAt(i)
            c.drawRect((view.left - dividerWidth).toFloat(), view.top.toFloat(), view.left.toFloat(), view.bottom.toFloat(), paint)
            c.drawRect((view.right + dividerWidth).toFloat(), view.top.toFloat(), view.right.toFloat(), view.bottom.toFloat(), paint)
            c.drawRect((view.left - dividerWidth).toFloat(), (view.top - dividerWidth).toFloat(), (view.right + dividerWidth).toFloat(), view.top.toFloat(), paint)
            c.drawRect((view.left - dividerWidth).toFloat(), view.bottom.toFloat(), (view.right + dividerWidth).toFloat(), (view.bottom + dividerWidth).toFloat(), paint)
        }
        c.restore()
    }
}
