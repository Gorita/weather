package com.mkcode.weatherapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mkcode.entity.LocationWeather
import com.mkcode.entity.LocationWeatherForecast
import com.mkcode.weatherapp.data.WeatherIcon
import com.mkcode.weatherapp.databinding.ItemWeatherForecastBinding
import com.mkcode.weatherapp.databinding.ItemWeatherHeaderBinding
import com.mkcode.weatherapp.databinding.ViewWeatherBinding

private const val HEADER_VIEW_TYPE = 1
private const val WEATHER_VIEW_TYPE = 2

class WeatherAdapter(
    diffCallback: DiffUtil.ItemCallback<LocationWeatherForecast>
) : ListAdapter<LocationWeatherForecast, RecyclerView.ViewHolder>(diffCallback) {
    override fun getItemCount(): Int =
        if (super.getItemCount() == 0) 0
        else super.getItemCount() + 1

    override fun getItemViewType(position: Int): Int =
        when (position) {
            0 -> HEADER_VIEW_TYPE
            else -> WEATHER_VIEW_TYPE
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            HEADER_VIEW_TYPE -> HeaderViewHolder(
                ItemWeatherHeaderBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> WeatherViewHolder(
                ItemWeatherForecastBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is WeatherViewHolder -> holder.bind(getItem(position - 1))
        }
    }


    class HeaderViewHolder(binding: ItemWeatherHeaderBinding) :
        RecyclerView.ViewHolder(binding.root)

    class WeatherViewHolder(private val binding: ItemWeatherForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(locationWeatherForecast: LocationWeatherForecast) {
            binding.locationTextView.text = locationWeatherForecast.locationName
            locationWeatherForecast.weathers.getOrNull(0)?.let { weather ->
                setViewWeatherData(binding.todayLayout, weather)
            }
            locationWeatherForecast.weathers.getOrNull(1)?.let { weather ->
                setViewWeatherData(binding.tomorrowLayout, weather)
            }
        }

        private fun setViewWeatherData(binding: ViewWeatherBinding, weather: LocationWeather) {
            with(binding) {
                WeatherIcon.getDrawableResId(weather.weatherStateAbbr)?.let {
                    weatherIconImageView.setImageResource(it)
                }
                weatherStateTextView.text = weather.weatherStateName
                temperatureTextView.text = String.format("%d℃", weather.theTemp.toLong())
                humidityTextView.text = String.format("%d%%", weather.humidity)
            }
        }
    }
}
