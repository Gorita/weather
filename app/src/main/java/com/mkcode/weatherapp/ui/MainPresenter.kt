package com.mkcode.weatherapp.ui

import com.mkcode.domain.GetSearchedLocationWeather
import com.mkcode.entity.LocationWeatherForecast
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val view: View,
    private val getSearchedLocationWeather: GetSearchedLocationWeather
) {
    fun onCreate() {
        view.setListener()
        view.initializeList()
        view.showLoading()
        requestWeather()
    }

    fun onRefresh() {
        requestWeather()
    }

    private fun requestWeather() {
        view.hideList()
        getSearchedLocationWeather.search(query = "se")
            .subscribe({
                view.showList()
                view.setLocationWeatherData(it)
                view.hideLoading()
            }, {
                it.printStackTrace()
            })
    }


    interface View {
        fun showList()
        fun hideList()
        fun setLocationWeatherData(locationWeathers: List<LocationWeatherForecast>)
        fun initializeList()
        fun setListener()
        fun showLoading()
        fun hideLoading()
    }
}
