package com.mkcode.weatherapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mkcode.domain.GetSearchedLocationMetaWeather
import com.mkcode.domain.ScheduleProvider
import com.mkcode.entity.LocationWeatherForecast
import com.mkcode.weatherapp.data.network.MetaWeatherApi
import com.mkcode.weatherapp.data.repository.MetaWeatherRepository
import com.mkcode.weatherapp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), MainPresenter.View {
    @Inject
    lateinit var presenter: MainPresenter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        presenter.onCreate()
    }

    override fun setListener() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            presenter.onRefresh()
        }
    }

    override fun initializeList() {
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = WeatherAdapter(WeatherDiffItemCallback).apply {
                addItemDecoration(
                    TableItemDecoration(this@MainActivity)
                )
            }
        }
    }

    override fun setLocationWeatherData(locationWeathers: List<LocationWeatherForecast>) {
        (binding.recyclerView.adapter as? WeatherAdapter)?.submitList(locationWeathers)
        binding.swipeRefreshLayout.isRefreshing = false
    }

    override fun hideList() {
        binding.recyclerView.isVisible = false
    }

    override fun showList() {
        binding.recyclerView.isVisible = true
    }

    override fun showLoading() {
        binding.progressBar.isVisible = true
    }

    override fun hideLoading() {
        binding.progressBar.isVisible = false
    }
}
