package com.mkcode.weatherapp.di

import com.mkcode.domain.ScheduleProvider
import com.mkcode.weatherapp.data.AppSchedulerProvider
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ScheduleModule {
    @Binds
    @Singleton
    abstract fun bindSchedulerProvider(
        provider: AppSchedulerProvider
    ): ScheduleProvider
}
