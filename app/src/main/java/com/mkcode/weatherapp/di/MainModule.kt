package com.mkcode.weatherapp.di

import android.app.Activity
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mkcode.domain.GetSearchedLocationMetaWeather
import com.mkcode.domain.GetSearchedLocationWeather
import com.mkcode.domain.ScheduleProvider
import com.mkcode.domain.WeatherRepository
import com.mkcode.weatherapp.ui.MainActivity
import com.mkcode.weatherapp.ui.MainPresenter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
object MainModule {
    @Provides
    @ActivityScoped
    fun provideView(activity: Activity): MainPresenter.View = activity as MainActivity

    @Provides
    @ActivityScoped
    fun provideRequestManager(activity: Activity): RequestManager =
        Glide.with(activity)
            .applyDefaultRequestOptions(
                RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
            )

    @Provides
    @ActivityScoped
    fun provideGetSearchedLocationWeather(
        scheduleProvider: ScheduleProvider,
        repository: WeatherRepository
    ): GetSearchedLocationWeather =
        GetSearchedLocationMetaWeather(scheduleProvider, repository)
}
