package com.mkcode.weatherapp.di

import com.mkcode.domain.WeatherRepository
import com.mkcode.weatherapp.data.repository.MetaWeatherRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindWeatherRepository(
        repository: MetaWeatherRepository
    ): WeatherRepository
}
