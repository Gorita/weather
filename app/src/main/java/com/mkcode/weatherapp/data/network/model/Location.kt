package com.mkcode.weatherapp.data.network.model

import com.google.gson.annotations.SerializedName
import com.mkcode.entity.Location as Entity


data class Location(
    @SerializedName("title")
    val title: String,
    @SerializedName("location_type")
    val locationType: String,
    @SerializedName("woeid")
    val woeid: Long,
    @SerializedName("latt_long")
    val lattLong: String
)

fun Location.toEntity(): Entity = Entity(title, locationType, woeid, lattLong)
