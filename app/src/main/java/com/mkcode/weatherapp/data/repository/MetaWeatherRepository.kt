package com.mkcode.weatherapp.data.repository

import com.mkcode.domain.WeatherRepository
import com.mkcode.entity.Location
import com.mkcode.entity.LocationWeather
import com.mkcode.weatherapp.data.network.MetaWeatherApi
import com.mkcode.weatherapp.data.network.model.toEntity
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class MetaWeatherRepository @Inject constructor(
    private val api: MetaWeatherApi
) : WeatherRepository {
    override fun searchLocation(query: String): Single<List<Location>> =
        api.searchLocations(query)
            .map { response ->
                response.map { it.toEntity() }
            }

    override fun getLocationWeather(woeid: Long): Single<List<LocationWeather>> =
        api.getLocationWeather(woeid)
            .map { response ->
                response.locationWeathers.map { it.toEntity() }
            }
}
