package com.mkcode.weatherapp.data.network

import com.mkcode.weatherapp.data.network.model.Location
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MetaWeatherApi {
    @GET("location/search/")
    fun searchLocations(
        @Query("query") query: String = "se",
    ): Single<List<Location>>

    @GET("location/{woeid}/")
    fun getLocationWeather(
        @Path("woeid") woeid: Long,
    ): Single<GetLocationWeatherResponse>
}
