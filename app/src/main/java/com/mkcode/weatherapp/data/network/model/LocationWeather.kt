package com.mkcode.weatherapp.data.network.model

import com.google.gson.annotations.SerializedName
import java.util.*
import com.mkcode.entity.LocationWeather as Entity


data class LocationWeather(
    @SerializedName("weather_state_name")
    val weatherStateName: String,
    @SerializedName("weather_state_abbr")
    val weatherStateAbbr: String,
    @SerializedName("the_temp")
    val theTemp: Double,
    @SerializedName("humidity")
    val humidity: Int,
    @SerializedName("applicable_date")
    val applicableDate: Date
)

fun LocationWeather.toEntity(): Entity = Entity(
    weatherStateName, weatherStateAbbr, theTemp, humidity, applicableDate
)
