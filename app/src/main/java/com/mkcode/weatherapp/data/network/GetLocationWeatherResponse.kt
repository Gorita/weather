package com.mkcode.weatherapp.data.network

import com.google.gson.annotations.SerializedName
import com.mkcode.weatherapp.data.network.model.LocationWeather

data class GetLocationWeatherResponse(
    @SerializedName("consolidated_weather")
    val locationWeathers: List<LocationWeather>
)
