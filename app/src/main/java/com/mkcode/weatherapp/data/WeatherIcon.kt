package com.mkcode.weatherapp.data

import com.mkcode.weatherapp.R

enum class WeatherIcon(val abbreviation: String, val resId: Int) {
    SNOW("sn", R.drawable.ic_snow),
    SLEET("sl", R.drawable.ic_sleet),
    HAIL("h", R.drawable.ic_hail),
    THUNDERSTORM("t", R.drawable.ic_thunderstorm),
    HEAVY_RAIN("hr", R.drawable.ic_heavy_rain),
    LIGHT_RAIN("lr", R.drawable.ic_light_rain),
    SHOWERS("s", R.drawable.ic_showers),
    HEAVY_CLOUD("hc", R.drawable.ic_heavy_cloud),
    LIGHT_CLOUD("lc", R.drawable.ic_light_cloud),
    CLEAR("c", R.drawable.ic_clear),
    ;

    companion object {
        fun get(abbr: String): WeatherIcon? = values().find { it.abbreviation == abbr }
        fun getDrawableResId(abbr: String): Int? = values().find { it.abbreviation == abbr }?.resId
    }
}
